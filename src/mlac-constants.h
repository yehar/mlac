// Defines for MLAC library.
// Copyright 2020 Olli Niemitalo (o@iki.fi)
// Constants needed by mlac-core, libmlac and by applications that use libmlac.

#pragma once

#define MLAC_BLOCK_NUM_BYTES 244
#define MLAC_BLOCK_MAX_NUM_SAMPLETUPLES 121
#define MLAC_BLOCK_MIN_NUM_SAMPLETUPLES 60
